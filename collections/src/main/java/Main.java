import be.kdg.java2.model.Student;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("jos",123);
        Student s2 = new Student("an",345);
        Student s3 = new Student("dirk",234);

        List<Student> students = new ArrayList<>();
        students.add(s1);
        students.add(s2);
        students.add(s3);

        Collections.sort(students);
        for (Student student : students) {
            System.out.println(student);
        }

        //Sorteren op nummer...
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                return s1.getNumber() - s2.getNumber();
            }
        });
        System.out.println("Op nummer gesorteerd:");
        for (Student student : students) {
            System.out.println(student);
        }

        Student s4 = new Student("Jef", 23456);
        Student s5 = new Student("Jef", 23456);

        if (s4.equals(s5)) {
            System.out.println(s4 + " en " + s5 + " zijn dezelfde!");
        } else {
            System.out.println(s4 + " en " + s5 + " zijn verschillend..");
        }

        Set<Student> geenDubbels = new TreeSet<>(students);
        geenDubbels.add(s4);
        geenDubbels.add(s5);
        System.out.println("Zonder dubbels:");
        for (Student geenDubbel : geenDubbels) {
            System.out.println(geenDubbel);
        }
    }
}
