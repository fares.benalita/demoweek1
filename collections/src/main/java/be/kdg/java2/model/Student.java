package be.kdg.java2.model;


import java.util.Objects;

public class Student implements Comparable<Student>{
    private String name;
    private int number;

    public Student(String name, int number) {
        setName(name);
        this.number = number;
    }

    public Student() {
        this("<noname>", -1);
    }

    public void setName(String name) {
        if (name==null||name.length()<2) {
            throw new IllegalArgumentException("Name must be at least 2 chars!");
        }
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public int compareTo(Student other) {
        if (this.name.compareTo(other.name)==0) {
            return this.number - other.number;
        }
        return this.name.compareTo(other.name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return number == student.number && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }
}
